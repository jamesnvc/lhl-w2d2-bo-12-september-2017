//
//  ViewController.m
//  FramesAndBounds
//
//  Created by James Cash on 12-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *pinkView;
@property (strong,nonatomic) UIView *cyanView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.cyanView = [[UIView alloc] initWithFrame:CGRectMake(20, 20, 100, 100)];
    self.cyanView.backgroundColor = [UIColor cyanColor];
    [self.pinkView addSubview:self.cyanView];

    // You can't assign to "part" of a struct, you need to change the whole thing
    // self.pinkView.frame.origin.y = 10;
    // self.pinkView.frame.origin = CGPointMake(self.pinkView.frame.origin.x, 10);
//    self.pinkView.frame = CGRectMake(self.pinkView.frame.origin.x, 10, self.pinkView.size.width, self.pinkView.size.height);
// Changing frame
//    self.pinkView.frame = CGRectOffset(self.pinkView.frame, 50, -200);
// Changing bounds
//      self.pinkView.bounds = CGRectOffset(self.pinkView.bounds, 20, 20);

//    self.pinkView.frame = CGRectInset(self.pinkView.frame, -50, -50);
        self.pinkView.bounds = CGRectInset(self.pinkView.bounds, -50, -50);

    UIView *green = [[UIView alloc] initWithFrame:CGRectZero];
    green.translatesAutoresizingMaskIntoConstraints = NO;
    green.backgroundColor = [UIColor greenColor];
    [self.pinkView addSubview:green];

    NSLayoutConstraint *height = [NSLayoutConstraint
                                  constraintWithItem:green
                                  attribute:NSLayoutAttributeHeight
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:self.cyanView
                                  attribute:NSLayoutAttributeHeight
                                  multiplier:1.0
                                  constant:0];
    height.active = YES;

    [green.widthAnchor constraintEqualToAnchor:self.cyanView.widthAnchor].active = YES;

    [green.leadingAnchor constraintEqualToAnchor:self.cyanView.leadingAnchor
                                        constant:30].active = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hitNumber:(UIButton *)sender {
    if (sender.tag == 1) {
        NSLog(@"First button");
    }
    NSLog(@"Button # %ld was pressed: %@", sender.tag, sender);
}


@end
